
public class Pyramids {
	
	
	
	public static double pyramidVolume(double height, double baseLength)
	{
		double baseArea = baseLength * baseLength; //baseArea Local Variable
		return height * baseArea / 3; 
	}

	public static void main(String[] args) { //Formal Parameter
		
		System.out.println("Volume: " +pyramidVolume(9, 10)); //9 & 10 Actual Parameters
		System.out.println("Expected: 300");
		System.out.println("Volume: " +pyramidVolume(0, 10)); //Also TOC
		System.out.println("Expected: 0");
	}
	

}
